//
//  MasterViewController.swift
//  NYCSchools
//
//  Created by Alan Ackroyd on 3/6/18.
//  Copyright © 2018 Alan Ackroyd. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet var appsTableView : UITableView!
    
    var detailViewController: DetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil


    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "NYC Schools"

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

 /*   @objc
    func insertNewObject(_ sender: Any) {
        let context = self.fetchedResultsController.managedObjectContext
        let newEvent = Event(context: context)
             
        // If appropriate, configure the new managed object.
        newEvent.school_name = "TEST!"

        // Save the context.
        do {
            try context.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }*/

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
            let object = fetchedResultsController.object(at: indexPath)
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let event = fetchedResultsController.object(at: indexPath)
        configureCell(cell, withEvent: event)
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false since everything is read only.
        return false
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let context = fetchedResultsController.managedObjectContext
            context.delete(fetchedResultsController.object(at: indexPath))
                
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func configureCell(_ cell: UITableViewCell, withEvent event: Event) {
        cell.textLabel!.text = event.school_name
    }

    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController<Event> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Sort by School Name
        let sortDescriptor = NSSortDescriptor(key: "school_name", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "NYC Schools")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
            
            let count = try self.managedObjectContext!.count(for: fetchRequest)
            
            // Intercept the data load, to see if there is nothing in the persistent storage.
            // If nothing exists, load it from JSON file.
            
            if ( count == 0 ) {
                self.loadInitialData()
                try _fetchedResultsController!.performFetch()
                self.appsTableView.reloadData()
            }
            
        } catch {
             // Replace this implementation with code to handle the error appropriately.
             // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             let nserror = error as NSError
             fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }    
    var _fetchedResultsController: NSFetchedResultsController<Event>? = nil

    
// Using this one since, it will be a bulk insert once only.
     
     func controllerDidChangeContent(controller: NSFetchedResultsController<NSFetchRequestResult>) {

     }
    
    // First time load of data from JSON file.
    
    func loadInitialData() {
        
        guard let jsonFile = Bundle.main.url(forResource: "nycschools", withExtension: "json") else {
            return
        }
        
        do {
            let jsonData = try Data(contentsOf: jsonFile)
            let json = try JSONSerialization.jsonObject(with: jsonData, options: [])
            let jsonArray = json as! [NSDictionary]
            
            for jsonDictionary in jsonArray {
                
                let context = self.fetchedResultsController.managedObjectContext
                let newEvent = Event(context: context)
                
                newEvent.dbn = jsonDictionary.value(forKey:"dbn") as? String
                newEvent.school_name = jsonDictionary.value(forKey:"school_name") as? String
                newEvent.boro = jsonDictionary.value(forKey:"boro") as? String
                newEvent.overview_paragraph = jsonDictionary.value(forKey:"overview_paragraph") as? String
                newEvent.neighborhood = jsonDictionary.value(forKey:"neighborhood") as? String
                newEvent.location = jsonDictionary.value(forKey:"location") as? String
                newEvent.phone_number = jsonDictionary.value(forKey:"phone_number") as? String
                newEvent.school_email = jsonDictionary.value(forKey:"school_email") as? String
                newEvent.website = jsonDictionary.value(forKey:"website") as? String
                newEvent.subway = jsonDictionary.value(forKey:"subway") as? String
                newEvent.bus = jsonDictionary.value(forKey:"bus") as? String
                newEvent.total_students = jsonDictionary.value(forKey:"total_students") as! Int16
                newEvent.graduation_rate = jsonDictionary.value(forKey:"graduation_rate") as! Float
                newEvent.attendance_rate = jsonDictionary.value(forKey:"attendance_rate") as! Float
                newEvent.pct_stu_enough_variety = jsonDictionary.value(forKey:"pct_stu_enough_variety") as! Float
                newEvent.college_career_rate = jsonDictionary.value(forKey:"college_career_rate") as! Float
                newEvent.pct_stu_safe = jsonDictionary.value(forKey:"pct_stu_safe") as! Float
                newEvent.sat_quantity = jsonDictionary.value(forKey:"sat_quantity") as? String
                newEvent.sat_reading = jsonDictionary.value(forKey:"sat_reading") as? String
                newEvent.sat_math = jsonDictionary.value(forKey:"sat_math") as? String
                newEvent.sat_writing = jsonDictionary.value(forKey:"sat_writing") as? String

                // Save the context.
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } catch {
            return
        }
        

    }

}

