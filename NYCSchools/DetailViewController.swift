//
//  DetailViewController.swift
//  NYCSchools
//
//  Created by Alan Ackroyd on 3/6/18.
//  Copyright © 2018 Alan Ackroyd. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    // Define some outlets to be used to display random pieces of data about the specific school.
    
    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var dbnLabel: UILabel!
    @IBOutlet weak var boroLabel: UILabel!
    @IBOutlet weak var neighborhoodLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UITextView!
    @IBOutlet weak var schoolEmailLabel: UITextView!
    @IBOutlet weak var websiteLabel: UITextView!
    @IBOutlet weak var subwayLabel: UILabel!
    @IBOutlet weak var busLabel: UILabel!
    @IBOutlet weak var totalStudentsLabel: UILabel!
    @IBOutlet weak var graduationRateLabel: UILabel!
    @IBOutlet weak var enoughVarietyLabel: UILabel!
    @IBOutlet weak var careerRateLabel: UILabel!
    @IBOutlet weak var stuSafeLabel: UILabel!
    @IBOutlet weak var satQuantityLabel: UILabel!
    @IBOutlet weak var satReadingLabel: UILabel!
    @IBOutlet weak var satMathLabel: UILabel!
    @IBOutlet weak var satWritingLabel: UILabel!
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            self.title = detail.school_name
            
            if let label = detailDescriptionLabel {
                label.text = detail.overview_paragraph
            }
            if let label = dbnLabel {
                label.text = detail.dbn
            }
            if let label = boroLabel {
                
                // Convert single letter to human radable.
                
                let boro: String! = detail.boro
                switch boro {
                case "X":
                    label.text = "Bronx"
                case "K":
                    label.text = "Brooklyn"
                case "Q":
                    label.text = "Queens"
                case "M":
                    label.text = "Manhattan"
                case "R":
                    label.text = "Staten Island"
                default:
                    label.text = detail.boro
                }
                
            }
            if let label = neighborhoodLabel {
                label.text = detail.neighborhood
            }
            if let label = phoneNumberLabel {
                label.text = detail.phone_number
                label.isEditable = false ;
                label.dataDetectorTypes = UIDataDetectorTypes.phoneNumber
            }
            if let label = schoolEmailLabel {
                label.text = detail.school_email
                label.isEditable = false ;
                label.dataDetectorTypes = UIDataDetectorTypes.all
            }
            if let label = websiteLabel {
                label.text = detail.website
                label.isEditable = false ;
                label.dataDetectorTypes = UIDataDetectorTypes.link
            }
            if let label = subwayLabel {
                label.text = detail.subway
            }
            if let label = busLabel {
                label.text = detail.bus
            }
            if let label = totalStudentsLabel {
                label.text = String(detail.total_students)
            }
            
            // Convert a few of the values to XX.YY% instead of 0.xxyyzzzzzzzzz
            
            if let label = graduationRateLabel {
                if ( detail.graduation_rate == 0 ) {
                    label.text = "N/A"
                } else {
                    label.text = String(round(detail.graduation_rate * 10000)/100) + "%"
                }
            }
            if let label = enoughVarietyLabel {
                if ( detail.pct_stu_enough_variety == 0 ) {
                    label.text = "N/A"
                } else {
                    label.text = String(round(detail.pct_stu_enough_variety * 10000)/100) + "%"
                }
            }
            if let label = careerRateLabel {
                if ( detail.college_career_rate == 0 ) {
                    label.text = "N/A"
                } else {
                    label.text = String(round(detail.college_career_rate * 10000)/100) + "%"
                }
            }
            if let label = stuSafeLabel {
                if ( detail.pct_stu_safe == 0 ) {
                    label.text = "N/A"
                } else {
                    label.text = String(round(detail.pct_stu_safe * 10000)/100) + "%"
                }
            }
            if let label = satQuantityLabel {
                label.text = detail.sat_quantity
            }
            if let label = locationLabel {
                label.text = detail.location
            }
            if let label = satReadingLabel {
                label.text = detail.sat_reading
            }
            if let label = satMathLabel {
                label.text = detail.sat_math
            }
            if let label = satWritingLabel {
                label.text = detail.sat_writing
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Event? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

